<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url()?>assets/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>

            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-th"></i>
                <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../layout/top-nav.html"><i class="fa fa-circle-o"></i> Manufacture</a></li>
                <li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> Spare Part</a></li>
                <li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> User</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Documents</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../charts/morris.html"><i class="fa fa-circle-o"></i>All Document</a></li>
                <li><a href="../charts/flot.html"><i class="fa fa-circle-o"></i> Unchecked</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-circle-o"></i> Unsigned</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Find Documents</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../charts/morris.html"><i class="fa fa-circle-o"></i>By Date</a></li>
                <li><a href="../charts/flot.html"><i class="fa fa-circle-o"></i> By User</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-circle-o"></i> By Attribute</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-circle-o"></i> Advance Search</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-table"></i> <span>Activity</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../tables/simple.html"><i class="fa fa-circle-o"></i> My Activity</a></li>
                <li><a href="../tables/data.html"><i class="fa fa-circle-o"></i> All Users</a></li>
              </ul>
            </li>
            <li>
              <a href="../calendar.html">
                <i class="fa fa-calendar"></i> <span>Configuration</span>
                <small class="label pull-right bg-red">3</small>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
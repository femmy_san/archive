<!doctype html>
<html class="no-js" lang="">

<head>
    <title>Video - Didit Heidiprasetyo</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/plugins/chosen/chosen.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/plugins/datatables/jquery.dataTables.css">
    <?php include('inc/load_top.php');?>

</head>

<!-- body -->

<body>
    <div class="app">
        <!-- top header -->
        <?php include('inc/header.php');?>
        <!-- /top header -->

        <section class="layout">
            <!-- sidebar menu -->
            <?php include('inc/sidebar.php');?>
            <!-- /sidebar menu -->

            <!-- main content -->
            <section class="main-content">

                <!-- content wrapper -->
                <div class="content-wrap">
                    
                    <!-- inner content wrapper -->
                    <div class="wrapper" style="padding: 25px;">
                        <div class="row">
                            <div class="col-sm-12">
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="javascript:;"><i class="ti-home mr5"></i>Dashboard</a>
                                    </li>
                                    <li class="active"><i class="ti-list mr5"></i> Video List</li>
                                </ol>
                            </div>
                        </div>
                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h5 style="float:left;"><i class="ti-list"></i>&nbsp;Video List</h5>
                                <a style="float:right;color:#fff;" class="btn btn-info" href="<?php echo $root_path.'video/add';?>"><i class="ti-plus"></i>&nbsp;Add New Video</a>
                                <span class="clearfix"></span>
                            </header>
                            <div class="panel-body">
                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable">
                                        <thead>
                                            <tr>
                                                <th width="40">No</th>
                                                <th width="90">Image</th>
                                                <th width="90">Title</th>
                                                <th width="90">Link</th>
                                                <th align="center" width="70">Status</th>
                                                <th width="120"  style="text-align:center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($list)):?>
                                                <?php foreach($list as $i => $row):?>
                                                    <tr>
                                                        <td><?php echo ($i+1);?></td>
                                                        <td><img src="<?php echo $row->get_img_src(true);?>" /></td>
                                                        <td><?php echo $row->title;?></td>
                                                        <td><?php echo $row->link;?></td>
                                                        <td style="text-align:center">
                                                            <?php $status = $row->status;?>
                                                            <?php if($status == 0):?>
                                                                <label class="label label-default">Not Published</label>
                                                            <?php else:?>
                                                                <label class="label label-success">Published</label>
                                                            <?php endif;?>
                                                        </td>
                                                        <td style="text-align:center">
                                                            <div class="btn-group" role="group" aria-label="...">
                                                              <a href="<?php echo $root_path.'video/update/'.$row->id;?>" type="button" class="btn btn-info"><i class="ti-pencil-alt"></i></a>
                                                              <a href="javsscript:" data-url="<?php echo $root_path.'video/delete/'.$row->id;?>" class="btn btn-danger btn-del"><i class="ti-trash"></i></a>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>

                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    <div id="modal-conf-delete" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation Delete Video</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure to remove this video?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="#" class="btn btn-danger btn-cdel">Delete</a>
              </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <?php include('inc/load_bottom.php');?>
    <script src="<?php echo base_url();?>assets/admin/plugins/chosen/chosen.jquery.min.js"></script>

    <script src="<?php echo base_url();?>assets/admin/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/bootstrap-datatables.js"></script>
    <script type="text/javascript">
        var demoDataTables = function () {
            return {
                init: function () {
                    $('.datatable').dataTable({
                        "sPaginationType": "bootstrap"
                    });

                    $('.chosen').chosen({
                        width: "80px"
                    });
                }
            };
        }();

        $(function () {
            "use strict";
            demoDataTables.init();
        });
        <?php if(!empty($success)):?>
            //toastr.options.type ="Success ";
            toastr.success('<?php echo $success;?>');
        <?php endif;?>
        $(document).ready(function(){
            $('.btn-del').click(function(){
                var url = $(this).attr('data-url');
                $('.btn-cdel').attr('href',url);
                $('#modal-conf-delete').modal('show');
            });

            $('.wysihtml5').wysihtml5({"font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                    "emphasis": true, //Italics, bold, etc. Default true
                    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                    "html": false, //Button which allows you to edit the generated HTML. Default false
                    "link": true, //Button to insert a link. Default true
                    "image": true, //Button to insert an image. Default true,
                    "color": false //Button to change color of font  
            });

        });
    </script>

</body>
<!-- /body -->

</html>

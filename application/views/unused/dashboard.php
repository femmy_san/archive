<!doctype html>
<html class="no-js" lang="">

<head>
    <title>Dashboard - Didit Heidiprasetyo</title>
    <?php include('inc/load_top.php');?>
    <link href="<?php echo base_url();?>assets/admin/plugins/jasny-fileupload/jasny-bootstrap.min.css" rel="stylesheet">
</head>

<!-- body -->

<body>
    <div class="app">
        <!-- top header -->
        <?php include('inc/header.php');?>
        <!-- /top header -->

        <section class="layout">
            <!-- sidebar menu -->
            <?php include('inc/sidebar.php');?>
            <!-- /sidebar menu -->

            <!-- main content -->
            <section class="main-content">

                <!-- content wrapper -->
                <div class="content-wrap">

                    <!-- inner content wrapper -->
                    <div class="wrapper">
                        <div class="row">
                            <div class="col-sm-12">
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="<?php echo $root_path.'dashboard/';?>"><i class="ti-home mr5"></i>Dashboard</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <section class="panel">
                                    <header class="panel-heading no-b" style="background-color:transparent;">
                                        <h5><i class="ti-layers-alt"></i>&nbsp;&nbsp;Homepage Showcase</h5>
                                    </header>
                                    <div class="panel-body">
                                         
                                    </div>
                                </section>

                            </div>
                            
                        </div>

                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>

    <?php include('inc/load_bottom.php');?>
    <script src="<?php echo base_url().'assets/admin/plugins/jasny-fileupload/jasny-bootstrap.min.js';?>"></script>

</body>
<!-- /body -->

</html>

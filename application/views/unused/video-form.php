<!doctype html>
<html class="no-js" lang="">

<head>
    <title>Video - Asoka</title>
    <?php include('inc/load_top.php');?>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/plugins/icheck/skins/minimal/green.css">  
    <link href="<?php echo base_url();?>assets/admin/plugins/jasny-fileupload/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/plugins/datepicker/datepicker.css" rel="stylesheet">
</head>

<!-- body -->

<body>
    <div class="app">
        <!-- top header -->
        <?php include('inc/header.php');?>
        <!-- /top header -->

        <section class="layout">
            <!-- sidebar menu -->
            <?php include('inc/sidebar.php');?>
            <!-- /sidebar menu -->

            <!-- main content -->
            <section class="main-content">

                <!-- content wrapper -->
                <div class="content-wrap">
                        
                    <!-- inner content wrapper -->
                    <div class="wrapper" style="padding: 25px;">
                        <div class="row">
                            <div class="col-sm-12">
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="<?php echo $root_path.'dashboard/';?>"><i class="ti-home mr5"></i>Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $root_path.'company/';?>"><i class="ti-window mr5"></i>Video</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <section class="panel">
                                    <header class="panel-heading no-b" style="background-color:transparent;">
                                        <h5><i class="ti-layers-alt"></i>&nbsp;&nbsp;Video Form</h5>
                                    </header>
                                    <div class="panel-body">
                                         <form role="form" data-toggle="validator" class="form-horizontal" action="<?php echo $root_path.'video/updater/'?>" method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="id" value="<?php echo $object!==false?$object->id:'';?>" />
                                            
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Image</label>
                                                <div class="col-sm-10">
                                                    <?php $class= $object!==false?'fileinput-exists':'fileinput-new'; ?>
                                                    <div class="fileinput <?php echo $class;?>" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 130px;">
                                                            <img src="http://placehold.it/1140x555" alt="..." style="width:100%;">                                                                
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                            <?php if($class=='fileinput-exists') : ?>
                                                            <img src="<?php echo $object->get_img_src();?>" alt="..." style="width:100%;height:100%;"> 
                                                            <?php endif;?>
                                                        </div>
                                                        <div>
                                                            <span class="btn btn-primary btn-file"><span class="fileinput-new">Browse Gambar</span><span class="fileinput-exists">Ubah</span><input type="file" name="image"></span>
                                                            <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <p class="help-block with-errors"><?php echo form_error('image');?></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Title</label>
                                                <div class="col-sm-10">
                                                    <input name="title" type="text" class="form-control" value="<?php echo $object!==false?$object->title:'';?>" required>
                                                    <p class="help-block with-errors"><?php echo form_error('sort');?></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Link</label>
                                                <div class="col-sm-10">
                                                    <input name="link" type="text" class="form-control" value="<?php echo $object!==false?$object->link:'';?>" required>
                                                    <p class="help-block with-errors"><?php echo form_error('sort');?></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Description</label>
                                                <div class="col-sm-10">
                                                    <textarea name="content" class="form-control" rows="5"><?php echo $object!==false?$object->content:'';?></textarea>
                                                    <p class="help-block with-errors"><?php echo form_error('content');?></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Status</label>
                                                <div class="col-sm-10">
                                                    <div class="checkbox">
                                                        <?php $status = $object!==false?$object->status:0; ?>
                                                        <input name="status" value="1" class="status" type="checkbox" id="status" <?php echo $status==1?'checked':'';?>>
                                                        <label for="status">Published</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">&nbsp;</label>
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-default">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </section>

                            </div>
                        </div>
                    </div>   
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>

    <?php include('inc/load_bottom.php');?>

    <div id="modal-delete-gallery" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to delete this image? &hellip;</p>
                </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="btn-conf-delete" type="button" class="btn btn-danger">Delete Image</button>
              </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <script src="<?php echo base_url();?>assets/admin/plugins/icheck/icheck.js"></script>
    <script src="<?php echo base_url().'assets/admin/plugins/jasny-fileupload/jasny-bootstrap.min.js';?>"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
        var url = base_url+'prasetyo/video/upload_img';
        function _(el){
            return document.getElementById(el);
        }
        function uploadFile(){
            var file = _("text_image").files[0];
            // alert(file.name+" | "+file.size+" | "+file.type);
            var formdata = new FormData();
            formdata.append("text_image", file);
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandler, false);
            ajax.addEventListener("load", completeHandler, false);
            ajax.addEventListener("error", errorHandler, false);
            ajax.addEventListener("abort", abortHandler, false);
            ajax.open("POST", url);
            ajax.send(formdata);
            $('div.progress').css('display','block');
            $('div.progress-bar').css('width','0%');
        }
        function progressHandler(event){
            // _("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
            var percent = (event.loaded / event.total) * 100;
            $('div.progress-bar').attr('aria-valuenow',percent);
            var w = percent+"%";
           $('div.progress-bar').css('width',w);
            // _("progressBar").value = Math.round(percent);
            // _("status").innerHTML = Math.round(percent)+"% uploaded... please wait";
        }
        function completeHandler(event){
            // $('div.progress').css('display','none');
            console.log(event.currentTarget.response);
            var data = event.currentTarget.response;
            $('input[name="link-image"]').val(data);

        }
        function errorHandler(event){
            // _("status").innerHTML = "Upload Failed";
        }
        function abortHandler(event){
            // _("status").innerHTML = "Upload Aborted";
        }



        $(document).ready(function(){
            $('#status').iCheck({
                labelHover: false,
                cursor: true
            });

            $('.wysihtml5').wysihtml5({"font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                    "emphasis": true, //Italics, bold, etc. Default true
                    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                    "html": true, //Button which allows you to edit the generated HTML. Default false
                    "link": true, //Button to insert a link. Default true
                    "image": true, //Button to insert an image. Default true,
                    "color": false //Button to change color of font  
            });

            $('.datepicker').datepicker();
            $('.fileinput-video').on('change.bs.fileinput',function(){
                uploadFile();
            });

            



        });
    </script>

</body>
<!-- /body -->

</html>

<!doctype html>
<html class="no-js" lang="">

<head>
    <title>Article - Asoka</title>
    <?php include('inc/load_top.php');?>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/plugins/icheck/skins/minimal/green.css">  
    <link href="<?php echo base_url();?>assets/admin/plugins/jasny-fileupload/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/plugins/datepicker/datepicker.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/plugins/dropzone/dropzone.css" rel="stylesheet">
    <style type="text/css">
        #my-awesome-dropzone{
            border: solid 1px #ccc;
            margin-bottom: 25px;
        }

        #my-awesome-dropzone h3{
            text-align: center;
        }

        .dz-preview{
            margin-bottom: 10px;
        }

        .btn-del-gallery{
            padding: 5px;
              position: absolute;
              bottom: 15px;
              left: 25px;

        }
        .dz-image-preview .thumbnail{
            margin-bottom: 5px;
            padding: 5px;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            border-radius: 0px;
        }



        .dz-image-preview .thumbnail img{
            width: 100%;
            height: 134px;
            cursor: "e-resize";
        }
    </style>
</head>

<!-- body -->

<body>
    <div class="app">
        <!-- top header -->
        <?php include('inc/header.php');?>
        <!-- /top header -->

        <section class="layout">
            <!-- sidebar menu -->
            <?php include('inc/sidebar.php');?>
            <!-- /sidebar menu -->

            <!-- main content -->
            <section class="main-content">

                <!-- content wrapper -->
                <div class="content-wrap">
                        
                    <!-- inner content wrapper -->
                    <div class="wrapper" style="padding: 25px;">
                        <div class="row">
                            <div class="col-sm-12">
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="<?php echo $root_path.'dashboard/';?>"><i class="ti-home mr5"></i>Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $root_path.'company/';?>"><i class="ti-window mr5"></i>Article</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <section class="panel">
                                    <header class="panel-heading no-b" style="background-color:transparent;">
                                        <h5><i class="ti-layers-alt"></i>&nbsp;&nbsp;Article Form</h5>
                                    </header>
                                    <div class="panel-body">
                                         <form role="form" data-toggle="validator" class="form-horizontal" action="<?php echo $root_path.'article/updater/'?>" method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="id" value="<?php echo $object!==false?$object->id:'';?>" />
                                            
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Image</label>
                                                <div class="col-sm-10">
                                                    <?php $class= $object!==false?'fileinput-exists':'fileinput-new'; ?>
                                                    <div class="fileinput <?php echo $class;?>" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://placehold.it/640x640" alt="..." style="width:100%;">                                                                
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                            <?php if($class=='fileinput-exists') : ?>
                                                            <img src="<?php echo $object->get_img_src();?>" alt="..." style="width:100%;"> 
                                                            <?php endif;?>
                                                        </div>
                                                        <div>
                                                            <span class="btn btn-primary btn-file"><span class="fileinput-new">Browse Gambar</span><span class="fileinput-exists">Ubah</span><input type="file" name="image"></span>
                                                            <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <p class="help-block with-errors"><?php echo form_error('image');?></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Title</label>
                                                <div class="col-sm-10">
                                                    <input name="title" type="text" class="form-control" value="<?php echo $object!==false?$object->title:'';?>" required>
                                                    <p class="help-block with-errors"><?php echo form_error('sort');?></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Category</label>
                                                <div class="col-sm-10">
                                                    <select name="category" class="form-control">
                                                        <?php $cat = $object!==false?$object->category:'';?>
                                                        <?php echo get_option_category($cat);?>
                                                    </select>
                                                    <p class="help-block with-errors"><?php echo form_error('sort');?></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Excerpt</label>
                                                <div class="col-sm-10">
                                                    <textarea name="excerpt" class="form-control" rows="5"><?php echo $object!==false?$object->excerpt:'';?></textarea>
                                                    <p class="help-block with-errors"><?php echo form_error('excerpt');?></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Content</label>
                                                <div class="col-sm-10">
                                                    <textarea name="content" class="form-control wysihtml5" rows="15"><?php echo $object!==false?$object->content:'';?></textarea>
                                                    <p class="help-block with-errors"><?php echo form_error('sort');?></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Publish Date</label>
                                                <div class="col-sm-10">
                                                    <?php $pdate = $object!==false?date('d-m-Y',$object->pdate):'';?>
                                                    <input name="pdate" data-date-format="dd-mm-yyyy" type="text" class="form-control datepicker" value="<?php echo $pdate;?>" required>
                                                    <p class="help-block with-errors"><?php echo form_error('sort');?></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Status</label>
                                                <div class="col-sm-10">
                                                    <div class="checkbox">
                                                        <?php $status = $object!==false?$object->status:0; ?>
                                                        <input name="status" value="1" class="status" type="checkbox" id="status" <?php echo $status==1?'checked':'';?>>
                                                        <label for="status">Published</label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <?php $feat = $object!==false?$object->feat:0; ?>
                                                        <input name="feat" value="1" class="status" type="checkbox" id="feat" <?php echo $feat==1?'checked':'';?>>
                                                        <label for="feat">Featured</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">&nbsp;</label>
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-default">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </section>

                            </div>
                        </div>
                        
                        <?php if($object!==false):?>
                        <div class="row">
                            <div class="col-sm-10">
                                <section class="panel">
                                    <header class="panel-heading no-b" style="background-color:transparent;">
                                        <h5><i class="ti-layers-alt"></i>&nbsp;&nbsp;Article Gallery</h5>
                                    </header>
                                    <div class="panel-body">
                                        <div class="mb25">
                                            <h5 style="float:left;" class="no-m text-uppercase">Upload <b>Article Gallery</b></h5>

                                            <a style="float:right;" href="<?php echo $root_path.'article/';?>" class="btn btn-danger">Back to Collection</a> 
                                            <span class="clearfix">&nbsp;</span>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-body no-p">
                                                <form id="my-awesome-dropzone" action="<?php echo $root_path.'article/gbatch_updater/';?>"  method="post" enctype="multipart/form-data" class="dropzone clearfix dz-clickable dz-started sortable">
                                                    <input type="hidden" name="article_id" value="<?php echo $object->id;?>">
                                                    <?php $glist = $object->get_gallery();?>
                                                    <input type="hidden" name="sort" value="<?php echo count($glist);?>">
                                                    <h3>Drag Image Here Or Click Here</h3>
                                                    <div id="my_dz" class="fallback">
                                                        <input name="image" type="file" multiple />
                                                    </div>

                                                </form>
                                                <div id="preview-gallery" class="row sortable">
                                                    <?php if(!empty($glist)):?>
                                                        <?php foreach($glist as $g):?>
                                                        <div id="<?php echo $g->id;?>" data-id="<?php echo $g->id;?>" class="col-xs-6 col-sm-3 col-md-3 dz-preview dz-image-preview">
                                                            <a href="#" class="thumbnail">
                                                                <img src="<?php echo $g->get_img_src(true);?>" alt="...">
                                                            </a>
                                                            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                                                            <div class="dz-error-message"><span data-dz-errormessage></span></div>
                                                            <div class="del-gallery"><a href="<?php echo $root_path.'article/delete_gallery/'.$g->id;?>" class="btn btn-danger btn-del-gallery"><span class="ti-trash">&nbsp;</span></a></div>
                                                        </div>
                                                        <?php endforeach;?>
                                                    <?php endif;?>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </section>

                            </div>
                        </div>
                        <?php endif;?>
                    </div>   
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>

    <?php include('inc/load_bottom.php');?>

    <div id="modal-delete-gallery" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to delete this image? &hellip;</p>
                </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="btn-conf-delete" type="button" class="btn btn-danger">Delete Image</button>
              </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <script src="<?php echo base_url();?>assets/admin/plugins/icheck/icheck.js"></script>
    <script src="<?php echo base_url().'assets/admin/plugins/jasny-fileupload/jasny-bootstrap.min.js';?>"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/admin/plugins/dropzone/dropzone.min.js';?>"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
        var url = base_url+'prasetyo/article/upload_img';
        function _(el){
            return document.getElementById(el);
        }
        function uploadFile(){
            var file = _("text_image").files[0];
            // alert(file.name+" | "+file.size+" | "+file.type);
            var formdata = new FormData();
            formdata.append("text_image", file);
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandler, false);
            ajax.addEventListener("load", completeHandler, false);
            ajax.addEventListener("error", errorHandler, false);
            ajax.addEventListener("abort", abortHandler, false);
            ajax.open("POST", url);
            ajax.send(formdata);
            $('div.progress').css('display','block');
            $('div.progress-bar').css('width','0%');
        }
        function progressHandler(event){
            // _("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
            var percent = (event.loaded / event.total) * 100;
            $('div.progress-bar').attr('aria-valuenow',percent);
            var w = percent+"%";
           $('div.progress-bar').css('width',w);
            // _("progressBar").value = Math.round(percent);
            // _("status").innerHTML = Math.round(percent)+"% uploaded... please wait";
        }
        function completeHandler(event){
            // $('div.progress').css('display','none');
            console.log(event.currentTarget.response);
            var data = event.currentTarget.response;
            $('input[name="link-image"]').val(data);

        }
        function errorHandler(event){
            // _("status").innerHTML = "Upload Failed";
        }
        function abortHandler(event){
            // _("status").innerHTML = "Upload Aborted";
        }

        function sorting(arr_str){
            var url = base_url+"remadja/article/sort_gallery/";
            var data = "str="+arr_str;
            var success = function(){

            }

            var beforeSend = function(){

            }

            var options = {
                url:url,
                data:data,
                success:success,
                beforeSend:beforeSend,
                type:"post",
                dataType:"json"
            }
            $.ajax(options);
        }

        $(document).ready(function(){
            $('#status').iCheck({
                labelHover: false,
                cursor: true
            });

            $('#feat').iCheck({
                labelHover: false,
                cursor: true
            });

            $('.wysihtml5').wysihtml5({"font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                    "emphasis": true, //Italics, bold, etc. Default true
                    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                    "html": true, //Button which allows you to edit the generated HTML. Default false
                    "link": true, //Button to insert a link. Default true
                    "image": true, //Button to insert an image. Default true,
                    "color": false //Button to change color of font  
            });

            $('.datepicker').datepicker();
            $('.fileinput-article').on('change.bs.fileinput',function(){
                uploadFile();
            });

            $( ".sortable" ).disableSelection();


            Dropzone.options.myAwesomeDropzone = {
                complete: function(event) {
                    var data = JSON.parse(event.xhr.responseText);

                    var id = data.id;
                    var url_thumb = data.url_thumb;
                    var url_delete = data.url_delete;
                    var $elm = $(event.previewElement);
                    $elm.attr('id',id);
                    $elm.attr('data-id',id);
                    $elm.find('img').attr('src',url_thumb);
                    $elm.find('a.btn-del-gallery').attr('href',url_delete);
                },
                previewsContainer:'#preview-gallery',
                previewTemplate:'<div class="col-xs-6 col-sm-3 col-md-3 dz-preview dz-image-preview">'+
                                    '<a href="#" class="thumbnail">'+
                                        '<img  data-dz-thumbnail>'+
                                    '</a>'+
                                    '<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>'+
                                    '<div class="dz-error-message"><span data-dz-errormessage></span></div>'+
                                    '<div class="del-gallery"><a href="" class="btn btn-danger btn-del-gallery"><span class="ti-trash">&nbsp;</span></a></div>'+
                                '</div>'

            };


            var myDropzone = new Dropzone("#my-awesome-dropzone");
            Dropzone.autoDiscover = false;
            myDropzone.on("queuecomplete", function(file) {
                var array = new Array();
                $('.dz-preview').each(function(){
                       var id = $(this).attr('data-id');
                       if(id!=''){
                            array.push(id);
                       }
                       
                });
                var arr_str = array.join(',');
                sorting(arr_str);
            });
            $( ".sortable" ).sortable({
                stop:function(event,ui){
                    console.log(ui);
                    console.log(event);
                    var array = new Array();
                    $('.dz-preview').each(function(){
                           var id = $(this).attr('data-id');
                           array.push(id);
                    });
                    var arr_str = array.join(',');
                    sorting(arr_str);

                }
            });
            $( ".sortable" ).disableSelection();
            var url_aja_del;
            $('body').on('click','.btn-del-gallery',function(e){
                var url = $(this).attr('href');
                $('#modal-delete-gallery').modal('show');
                url_aja_del = url;
                e.preventDefault();
            });

            $('#btn-conf-delete').click(function(e){
                    var options = {
                        type:"post",
                        dataType:"json",
                        url:url_aja_del,
                        success:function(data){
                            if(data.status){
                                var id = data.id;
                                $('#'+id).remove();
                                $('#modal-delete-gallery').modal('hide');
                            }
                        }, beforeSend:function(){

                        } 
                    }

                    $.ajax(options);
                    e.preventDefault();
            });

        });
    </script>

</body>
<!-- /body -->

</html>

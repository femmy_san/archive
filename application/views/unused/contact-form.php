<!doctype html>
<html class="no-js" lang="">

<head>
    <title>Contact - Didit Heidiprasetyo</title>
    <?php include('inc/load_top.php');?>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/plugins/icheck/skins/minimal/green.css">  
    <link href="<?php echo base_url();?>assets/admin/plugins/jasny-fileupload/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet">
</head>

<!-- body -->

<body>
    <div class="app">
        <!-- top header -->
        <?php include('inc/header.php');?>
        <!-- /top header -->

        <section class="layout">
            <!-- sidebar menu -->
            <?php include('inc/sidebar.php');?>
            <!-- /sidebar menu -->

            <!-- main content -->
            <section class="main-content">

                <!-- content wrapper -->
                <div class="content-wrap">
                        
                    <!-- inner content wrapper -->
                    <div class="wrapper" style="padding: 25px;">
                        <div class="row">
                            <div class="col-sm-10">
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="<?php echo $root_path.'dashboard/';?>"><i class="ti-home mr5"></i>Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $root_path.'about/';?>"><i class="ti-window mr5"></i>Contact</a>
                                    </li>
                                    
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <section class="panel">
                                    <header class="panel-heading no-b" style="background-color:transparent;">
                                        <h5><i class="ti-layers-alt"></i>&nbsp;&nbsp;Content Contact</h5>
                                    </header>
                                    <div class="panel-body">
                                         <form role="form" data-toggle="validator"  action="<?php echo $root_path.'contact/updater/'?>" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label class="control-label">Personal Info</label>
                                                <textarea name="personal_info" class="form-control wysihtml5" rows="10"><?php echo get_meta('personal_info');?></textarea>
                                                <p class="help-block with-errors"><?php echo form_error('sort');?></p>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Press Info</label>
                                                <textarea name="press_info" class="form-control wysihtml5" rows="10"><?php echo get_meta('press_info');?></textarea>
                                                <p class="help-block with-errors"><?php echo form_error('press_info');?></p>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Satkaara</label>
                                                <textarea name="satkaara" class="form-control wysihtml5" rows="10"><?php echo get_meta('satkaara');?></textarea>
                                                <p class="help-block with-errors"><?php echo form_error('satkaara');?></p>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Facebook Link</label>
                                                <input type="text" class="form-control" name="fb_link" value="<?php echo get_meta('fb_link');?>">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Instagram Link</label>
                                                 <input type="text" class="form-control" name="ig_link" value="<?php echo get_meta('ig_link');?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Tumblr Link</label>
                                                <input type="text" class="form-control" name="tblr_link" value="<?php echo get_meta('tblr_link');?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Pinterest Link</label>
                                                <input type="text" class="form-control" name="pin_link" value="<?php echo get_meta('pin_link');?>">
                                            </div>
                                            <div class="form-group">
                                                <!-- <label class="col-sm-2 control-label">&nbsp;</label> -->
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-default">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </section>

                            </div>
                        </div>
                    </div>   
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>

    <?php include('inc/load_bottom.php');?>
    <script src="<?php echo base_url();?>assets/admin/plugins/icheck/icheck.js"></script>
    <script src="<?php echo base_url().'assets/admin/plugins/jasny-fileupload/jasny-bootstrap.min.js';?>"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
   
    <script type="text/javascript">
        $(document).ready(function(){
            $('#status').iCheck({
                labelHover: false,
                cursor: true
            });
            <?php if(!empty($success)):?>
            //toastr.options.type ="Success ";
            toastr.success('<?php echo $success;?>');
            <?php endif;?>
            <?php if(!empty($error)):?>
                //toastr.options.type ="Success ";
                toastr.error('<?php echo $error;?>');
            <?php endif;?>
            $('.wysihtml5').wysihtml5({"font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                    "emphasis": true, //Italics, bold, etc. Default true
                    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                    "html": true, //Button which allows you to edit the generated HTML. Default false
                    "link": true, //Button to insert a link. Default true
                    "image": true, //Button to insert an image. Default true,
                    "color": false //Button to change color of font  
            });

        });
    </script>

</body>
<!-- /body -->

</html>

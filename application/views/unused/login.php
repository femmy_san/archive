<!doctype html>
<html class="no-js" lang="">

<head>
    <title>Login - Asoka</title>
    <?php include('inc/load_top.php');?>
</head>

<!-- body -->

<body>

    <div class="cover" style="background-image: url(img/cover3.jpg)"></div>

    <div class="overlay"></div>

    <div class="center-wrapper">
        <div class="center-content">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                	<h3 style="color:#fff;margin-bottom:35px;">Asoka Backend System</h3>
                    <section class="panel bg-white no-b">
                        <h3 style="text-align:center;padding:25px 15px;color:#333;">Administrator Sign In</h3>
                        <div class="p15">  
                            <form role="form" action="<?php echo $page_path.'process/';?>" method="post">
                                <input name="username" type="text" class="form-control input-lg mb25" placeholder="Username" autofocus>
                                <input name="password" type="password" class="form-control input-lg mb25" placeholder="Password">
                                
                                <button class="btn btn-lg btn-block" type="submit">Sign in</button>
                            </form>
                        </div>
                    </section>
                    <p class="text-center" style="color:#fff;">
                        Copyright &copy;
                        <span id="year" class="mr5"></span>
                        <span></span>
                    </p>
                </div>
            </div>
        
        </div>
    </div>
    <script type="text/javascript">
        var el = document.getElementById("year"),
            year = (new Date().getFullYear());
        el.innerHTML = year;
    </script>
</body>
<!-- /body -->

</html>

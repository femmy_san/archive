<!-- meta -->
    <meta charset="utf-8">
    <meta name="description" content="Flat, Clean, Responsive, application admin template built with bootstrap 3">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <!-- /meta -->

    

    <!-- page level plugin styles -->
    <!-- /page level plugin styles -->

    <!-- core styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/animate.min.css">
    <!-- /core styles -->

    <!-- template styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/palette.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/font.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/plugins/toastr/toastr.min.css">
    <!-- template styles -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- load modernizer -->
    <script src="<?php echo base_url();?>assets/admin/js/modernizr.js"></script>

     <script type="text/javascript">var base_url="<?php echo base_url();?>"</script>
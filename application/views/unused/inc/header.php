<header class="header header-fixed navbar">

            <div class="brand">
                <!-- toggle offscreen menu -->
                <a href="javascript:;" class="ti-menu off-left visible-xs" data-toggle="offscreen" data-move="ltr"></a>
                <!-- /toggle offscreen menu -->

                <!-- logo -->
                <a href="index.html" class="navbar-brand">
                    <span class="heading-font">
                        Asoka Backend
                    </span>
                </a>
                <!-- /logo -->
            </div>

            <ul class="nav navbar-nav">
                <li class="hidden-xs">
                    <!-- toggle small menu -->
                    <a href="javascript:;" class="toggle-sidebar">
                        <i class="ti-menu"></i>
                    </a>
                    <!-- /toggle small menu -->
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">

                <li class="off-right">
                    <a href="javascript:;" data-toggle="dropdown">
                        <!-- <img src="img/faceless.jpg" class="header-avatar img-circle" alt="user" title="user"> -->
                        <span class="hidden-xs ml10"><?php echo $adm->name;?></span>&nbsp;&nbsp;
                        <i class="ti-angle-down ti-caret hidden-xs"></i>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight">
<!--                         <li>
                            <a href="<?php echo $root_path.'admin/update_pass';?>">Change Password</a>
                        </li> -->
                        <li>
                            <a href="<?php echo $root_path.'login/off/';?>">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </header>
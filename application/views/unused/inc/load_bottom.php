<!-- core scripts -->
    <script src="<?php echo base_url();?>assets/admin/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/jquery.easing.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/appear/jquery.appear.js"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/jquery.placeholder.js"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/fastclick.js"></script>
    <script src="<?php echo base_url();?>assets/admin/plugins/toastr/toastr.min.js"></script>
    <!-- /core scripts -->

    <!-- page level scripts -->
    <!-- /page level scripts -->

    <!-- template scripts -->
    <script src="<?php echo base_url();?>assets/admin/js/offscreen.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/main.js"></script>
    <!-- /template scripts -->

    <!-- page script -->
    <!-- /page script -->

    <?php if(!empty($success)):?>
            
    toastr.success('<?php echo $success;?>');
    <?php endif;?>


    <?php if(!empty($error)):?>
            
    toastr.error('<?php echo $error;?>');
    <?php endif;?>
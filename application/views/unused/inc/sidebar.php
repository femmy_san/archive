<aside class="sidebar offscreen-left">
                <!-- main navigation -->
                <nav class="main-navigation" data-height="auto" data-size="6px" data-distance="0" data-rail-visible="true" data-wheel-step="10">
                    <p class="nav-title">MENU</p>
                    <ul class="nav">
                        <!-- dashboard -->
                        <li>
                            <a href="<?php echo $root_path.'dashboard/';?>">
                                <i class="ti-home"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $root_path.'article/';?>">
                                <i class="ti-layout-grid3-alt"></i>
                                <span>Article</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $root_path.'video/';?>">
                                <i class="ti-clipboard"></i>
                                <span>Video</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $root_path.'contact/';?>">
                                <i class="ti-announcement"></i>
                                <span>Contact</span>
                            </a>
                        </li>
                        <!-- /dashboard -->

   
                </nav>
            </aside>
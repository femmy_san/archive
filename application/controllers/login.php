<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	private $_root_path;

	private $_page_path;

	private $_page_name = 'Login';

	private $_admin_id;

	private $_data_load = array();

// -------------------------- Default Property -------------------------------------//	

	public function __construct(){
		parent::__construct();
		$this->_init();
	}

	private function _init(){
		$this->_root_path = base_url();
		$this->_page_path = $this->_root_path.'login/';
		$this->_admin_id  = $this->session->userdata('admin_id');
		
		$this->_data_load['root_path'] 	= $this->_root_path;
		$this->_data_load['page_path'] 	= $this->_page_path;
		$this->_data_load['page_name'] 	= $this->_page_name.' | '.GG_APPNAME;
	}
	
	public function index(){
		if(!empty($this->admin_id)){
			redirect($this->root_path.'dashboard/');
		}
		$this->load->view('login',$this->_data_load);
	}

	public function process(){
		$user = $this->input->post('username',true);
		$pass = $this->input->post('password',true);
		$this->load->model('mod_admin','adm',true,'',$user);
		$data = $this->adm->data;
		$hashpass = sha1(SALT_ADM.$pass);
		//die($hashpass);
		if(!empty($data)){
			if($this->adm->password == $hashpass){
				$data['last_login'] = time();
				$data['mdate']		= time();
				$this->adm->set_value($data);;
				$this->session->set_userdata('admin_id',$this->adm->id);
				//die($this->adm->id.'-----------------dsadsadsadsa--------------');
				redirect($this->_root_path.'dashboard/');
			} else {
				$this->data_load['error'] = 'Username and password not match';
				$this->load->view('admin/login',$this->_data_load);
			}
		} else {
			$this->data_load['error'] = 'Account not found';
			$this->load->view('admin/login',$this->_data_load);
		}
	}

	public function off(){
        $this->session->sess_destroy();
        redirect($this->_root_path);
    }
}
<?php
    function strip_single($tag,$string){
        $string=preg_replace('/<'.$tag.'[^>]*>/i', '', $string);
        $string=preg_replace('/<\/'.$tag.'>/i', '', $string);
        return $string;
    }
    
    function get_category_name($id=0){
         $CI =& get_instance();
         $row = $CI->db->get_where('category',array('id'=>$id))->row();
         if(!empty($row))
         {
              return $row->name;
         }
         return null;
       
    }
    
    function get_tipe_material($tipe=0){
        $CI =& get_instance();
        $row = $CI->db->get_where('tipe_material',array('id'=>$tipe))->row();
        if(!empty($row))
         {
              return $row->tipe;
         }
         return null;
    }
    
    function get_login_error(){
        $CI =& get_instance();
        return $CI->session->flashdata('error_login');
    }
    
    function get_login_success(){
        $CI =& get_instance();
        return $CI->session->flashdata('success_login');
    }
    
    function get_category_bytype($type){
        $CI =& get_instance();
        $CI->load->model('mod_category');
        return $CI->mod_category->get_bytype($type);
    }
    
    function get_str_month($int){
        $result     = array('Month Publish','January','February','March','April','May','June','July'
        ,'August','September','October','November','December');
        return $result[$int];
    }
    
    function get_province_by_city($city){
        $CI =& get_instance();
        $row = $CI->db->get_where('city',array('id'=>$city))->row();
        return $row->province_id;
    }
    
    function no_order($id=''){
            $return = $id;
            $max_len = 5; 
            $len_id = strlen($id);
            //die($len_id.'---dsadsadas');
            while($max_len > $len_id)
            {
                $return = '0'.$return;
                $len_id++;
            }
            return  '#'.$return;

    }
    
    function get_province_name($province){
        $CI =& get_instance();
        $row = $CI->db->get_where('province',array('id'=>$province))->row();
        return $row->name;
    }
    
    function get_city_name($city){
        $CI =& get_instance();
        $row = $CI->db->get_where('city',array('id'=>$city))->row();
        return $row->name;
    }
    
    function resize_move($source,$path){
        $CI =& get_instance();
        $filename = end(explode('/',$source));
        $CI->load->library('image_lib'); 
        $cfg_rsz_med = array();
        $cfg_rsz_med['image_library']   = 'gd2';
        $cfg_rsz_med['source_image']    = $source;
        $cfg_rsz_med['new_image']       = $path.$filename;
        $cfg_rsz_med['maintain_ratio']  = TRUE;
        $cfg_rsz_med['width']           = 320;

        $CI->image_lib->initialize($cfg_rsz_med);
        $return = array();
        $return['error'] = false;
        if (!$CI->image_lib->resize())
        {
            $return['error'] = 'Image Medium : '.$CI->image_lib->display_errors();
        }
        return $return;           
    }

    function get_meta($key=''){
        $CI =& get_instance();
        return $CI->mod_meta->get($key);
    }

    
?>
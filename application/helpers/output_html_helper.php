<?php
function breadcrump(){
    $CI =& get_instance();
    $breadcrump = '<ul class="breadcrumb">';
    $array_segment = $CI->uri->segment_array();
    if(!empty($array_segment))
    {
        $len_arr = count($array_segment);
        $link = base_url().'admin/';
        foreach($array_segment as $key => $val)
        {
            $name =  ucfirst(str_replace('_', ' ', $val));
            if($key == $len_arr)
            {
                $breadcrump.='<li class="active">'.$name.'</li>';
            }
            elseif($key == 1)
            {
                $breadcrump.='<li><a href="'.$link.'"><span class="awe-home"></span>'.$name.'</a></li>';
            }
            else
            {
                $link.= $val.'/';
                $breadcrump.='<li><a href="'.$link.'">'.$name.'</a></li>';
            }
            
        }
    }
    $breadcrump.='</ul>';
    return $breadcrump;        
}

function get_option($table,$selected=''){
    $return = '';
    $CI =& get_instance();
    $query     = $CI->db->get($table);
    $result    = $query->result(); 
    //die(print_r($result));
    if(!empty($result))
    {
        foreach ($result as $item) {
            if($item->id == $selected)
            {
                $return.='<option value="'.$item->id.'" selected="selected">'.$item->name.'</option>';
            }
            else
            {
                $return.='<option value="'.$item->id.'">'.$item->name.'</option>';
            }
        }
    }

    return $return;
}

function get_option_category($selected=''){
    $return = '';
    $array_cat = array('0'=>'Category','1'=>'Places','2'=>'Yumms','3'=>'Save The World');
    foreach ($array_cat as $key=> $cat) {
        if($selected == $key ){
            $return.='<option value="'.$key.'" selected>'.$cat.'</option>';
        } else {
            $return.='<option value="'.$key.'">'.$cat.'</option>';
        }
    }
    return $return;
}


function get_option_city($province=1,$selected=''){
    $return = '';
    $CI =& get_instance();
    $result     = $CI->db->get_where('city',array('province_id'=>$province))->result();
    if(!empty($result))
    {
        foreach ($result as $item) {
            if($item->id == $selected)
            {
                $return.='<option value="'.$item->id.'" selected="selected">'.$item->name.'</option>';
            }
            else
            {
                $return.='<option value="'.$item->id.'">'.$item->name.'</option>';
            }
        }
    }

    return $return;
}

function get_option_month($month=0){
    $return = '';
    $CI =& get_instance();
    $result     = array('Month Publish','January','February','March','April','May','June','July'
        ,'August','September','October','November','December');
    foreach ($result as $k => $v) {
            if($k == $month)
            {
                $return.='<option value="'.$k.'" selected="selected">'.$v.'</option>';
            }
            else
            {
                $return.='<option value="'.$k.'">'.$v.'</option>';
            }
        }


    return $return;
}

?>

-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 09 Apr 2015 pada 10.40
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `asoka`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `username` varchar(64) NOT NULL,
  `level` tinyint(4) NOT NULL,
  `password` text NOT NULL,
  `last_login` int(11) NOT NULL,
  `ctime` int(11) NOT NULL,
  `mtime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `name`, `username`, `level`, `password`, `last_login`, `ctime`, `mtime`) VALUES
(1, 'Administrator', 'asokaadm', 1, '596df895e18410b94fd1f83dd33164b7b130fa6c', 1422696945, 1422696945, 1422713365);

-- --------------------------------------------------------

--
-- Struktur dari tabel `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `excerpt` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `category` int(11) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `feat` int(11) NOT NULL,
  `cdate` int(11) NOT NULL,
  `pdate` int(11) NOT NULL,
  `mdate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `article`
--

INSERT INTO `article` (`id`, `title`, `slug`, `excerpt`, `image`, `category`, `content`, `status`, `feat`, `cdate`, `pdate`, `mdate`) VALUES
(8, 'Basic Espresso', 'basic_espresso', 'Tale quando prompta at vel, his audire dolorem ceteros ut, no nam mundi iuvaret. Ut molestie delicata constituam his. Ad his oblique vocibus elaboraret', '1428565634.png', 1, '<div>Regione appetere usu te, cum unum splendide in, vis quando periculis eu. Ne quis elit vim, habeo facer no vix. Ullum periculis eu eam, ei quo nonumy democritum, iriure persius nominavi his at. Vis cu nostrum molestie, eam ea homero dissentias eloquentiam. Eu mei idque singulis, solum iriure veritus sea no.</div><div><br></div><div>Duo augue impetus nonumes cu, mei ea quis nulla epicurei. Exerci scripserit te mea, modo saepe scaevola per at. Quis vidit scaevola eos at, graeco aperiri philosophia id usu. Eu iusto homero nec, nam ei mucius option epicurei. Et mea nominavi imperdiet, mel erat legere cu, id sed oblique torquatos. Omnis tempor interesset id pro, vix te aperiri phaedrum singulis, mollis fabellas argumentum te per.</div><div><br></div><div>Usu amet intellegat instructior at. Te partem albucius mnesarchum mel, pri dicam quando petentium ad, cu quo offendit indoctum. Audire eripuit mnesarchum eu his, te nec quas tacimates, eum cu fugit pertinax. Est magna falli maiestatis ei. Cu eos minimum appareat detraxit.</div><div><br></div><div>Duo ne erroribus honestatis. Quo movet tollit omittantur te. Ex aeque dolor qui, id velit lucilius mediocritatem cum. Blandit accusata ut duo, nobis reformidans ea quo.</div><div><br></div>', 1, 1, 1428565634, 1428858000, 1428565634);

-- --------------------------------------------------------

--
-- Struktur dari tabel `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` text NOT NULL,
  `key` varchar(96) NOT NULL,
  `type` int(11) NOT NULL,
  `value` text NOT NULL,
  `option` text NOT NULL,
  `desc` text NOT NULL,
  `sort` int(11) NOT NULL,
  `group` varchar(96) DEFAULT '0',
  `cdate` int(11) NOT NULL,
  `mdate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='config' AUTO_INCREMENT=69 ;

--
-- Dumping data untuk tabel `config`
--

INSERT INTO `config` (`id`, `label`, `key`, `type`, `value`, `option`, `desc`, `sort`, `group`, `cdate`, `mdate`) VALUES
(60, 'About', 'about', 3, 'Attended both Parsons School of Design New York and Paris respectively, he received Silver Thimble Award in 2006 and Bachelor of Fine Arts degree in Fashion Design at 2007. With a passion for structured draperies and intricate embellishments, in addition to sophisticated workmanship, Hediprasetyo remains effortless yet refined in creating his clothing. His designs are to accentuate understated details. It is the intimate luxury of one-on-one relationship with a significant individual that makes his fondness of couture more realistic.', '', '', 0, '1', 1417331772, 1425921884),
(61, 'Personal info', 'personal_info', 3, '<a target="" rel="">contact@didithediprasetyo.com</a><br>\r\n        Ph. +62 878 850 22223', '', '', 0, '1', 1417331772, 1425921834),
(62, 'Press Info', 'press_info', 3, 'Alexandre Boulais Communication<br>\r\n        <a target="" rel="">contact@aboulais.com</a><br>\r\n        Ph. +33 1 53 20 99 98<br>\r\n        24, rue de Cha?teaudun 75009 Paris<br>\r\n        France', '', '', 0, '1', 1417331772, 1425921834),
(63, 'SATKAARA', 'satkaara', 3, '<a target="" rel="">ruthandriani.pm@satkaara.net</a><br>\r\n        Ph. +62 815 138 78007<br>\r\n        Graha Morzell, Jalan Pejaten Raya No.22 Jakarta 12540<br>\r\n        Indonesia', '', '', 0, '1', 1417331772, 1425921834),
(64, 'Facebook Link', 'fb_link', 3, 'https://www.facebook.com/pages/Didit-Hediprasetyo/879043602121246', '', '', 0, '1', 1417331772, 1425921834),
(65, 'Instagram Link', 'ig_link', 3, 'https://instagram.com/didithediprasetyo', '', '', 0, '1', 1417331772, 1425921834),
(66, 'Pinterest Link', 'pin_link', 3, 'https://www.pinterest.com/k123win/didit-hediprasetyo/', '', '', 0, '1', 1417331772, 1425921834),
(67, 'Tumbler Link', 'tblr_link', 3, 'https://www.tumblr.com/search/didit%20couture', '', '', 0, '1', 1417331772, 1425921834),
(68, 'Showcase', 'showcase', 5, '1426235509.jpg', '', '', 0, '1', 1417331772, 1426235509);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `sort` tinyint(4) NOT NULL,
  `mdate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data untuk tabel `gallery`
--

INSERT INTO `gallery` (`id`, `article_id`, `image`, `sort`, `mdate`) VALUES
(9, 3, '1426061081.jpg', 1, 1426061081),
(10, 3, '1426061091.jpg', 2, 1426061091),
(11, 3, '1426061100.jpg', 3, 1426061100),
(12, 3, '1426061109.jpg', 4, 1426061109),
(13, 3, '1426061117.jpg', 5, 1426061117),
(14, 3, '1426061126.jpg', 6, 1426061126),
(15, 3, '1426061135.jpg', 7, 1426061135),
(16, 3, '1426061144.jpg', 8, 1426061144),
(19, 8, '1428565693.png', 0, 1428565693),
(20, 8, '1428565767.png', 1, 1428565767);

-- --------------------------------------------------------

--
-- Struktur dari tabel `meta_group`
--

CREATE TABLE IF NOT EXISTS `meta_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `separated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `meta_group`
--

INSERT INTO `meta_group` (`id`, `name`, `active`, `separated`) VALUES
(1, 'home', 1, 0),
(2, 'playlist', 1, 0),
(3, 'info', 1, 0),
(4, 'landing', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL,
  `cdate` int(11) NOT NULL,
  `mdate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `video`
--

INSERT INTO `video` (`id`, `title`, `slug`, `link`, `image`, `content`, `status`, `cdate`, `mdate`) VALUES
(1, 'Manual Brewing', 'manual_brewing', '', '1428568476.png', 'lorem ipsum dolor sir amet', 1, 1428568476, 1428568564),
(2, 'Advance Espresso', 'advance_espresso', 'https://www.youtube.com/watch?v=NTKwzRAdY7w', '1428568738.png', 'Lorem Ipsum', 1, 1428568738, 1428568738);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

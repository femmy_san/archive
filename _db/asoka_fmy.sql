-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 10 Apr 2015 pada 03.20
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `asoka`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `username` varchar(64) NOT NULL,
  `level` tinyint(4) NOT NULL,
  `password` text NOT NULL,
  `last_login` int(11) NOT NULL,
  `ctime` int(11) NOT NULL,
  `mtime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `name`, `username`, `level`, `password`, `last_login`, `ctime`, `mtime`) VALUES
(1, 'Administrator', 'asokaadm', 1, '596df895e18410b94fd1f83dd33164b7b130fa6c', 1422696945, 1422696945, 1422713365);

-- --------------------------------------------------------

--
-- Struktur dari tabel `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `excerpt` varchar(240) NOT NULL,
  `image` text NOT NULL,
  `category` int(11) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `feat` tinyint(1) NOT NULL,
  `pdate` int(11) NOT NULL,
  `cdate` int(11) NOT NULL,
  `mdate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `article`
--

INSERT INTO `article` (`id`, `title`, `slug`, `excerpt`, `image`, `category`, `content`, `status`, `feat`, `pdate`, `cdate`, `mdate`) VALUES
(8, 'At Eum Graeco Mediocritatem', 'at_eum_graeco_mediocritatem', 'At vel iusto homero, ius id scaevola accommodare. In omnis vitae his, ex vidit theophrastus vix, eu nisl impedit mea. Graeco melius ne mea, eam fabulas comprehensam te. Qui putent principes necessitatibus cu, nam cu causae electram definieb', '1426654303.jpg', 1, '<div>At vel iusto homero, ius id scaevola accommodare. In omnis vitae his, ex vidit theophrastus vix, eu nisl impedit mea. Graeco melius ne mea, eam fabulas comprehensam te. Qui putent principes necessitatibus cu, nam cu causae electram definiebas.</div><div><br></div><div>At eum graeco mediocritatem, mei quot porro id. Ex per agam corpora principes. In ius ignota discere appellantur, an fabulas volumus cum, pro movet accumsan adversarium no. Erant nemore ornatus eu pro. Est at iuvaret tibique constituto. Iriure apeirian in nec.</div><div><br></div><div>Est et facete offendit mediocritatem, per debet timeam no. Ei autem forensibus duo, solet tincidunt et vel. Usu elitr tamquam in, ex mea posse convenire. Cu duo solet munere animal, everti laoreet scripserit mea ne, vim insolens delicata erroribus ut. Viris vitae in duo. Eam cu volumus suscipit. Id detracto accusamus pro.</div><div><br></div><div>Usu sint voluptaria reprehendunt an. Eu usu torquatos scriptorem, ad duo magna postulant, et pri mentitum oportere. Ei dolor ubique verterem has, modus primis fierent qui ei. Soleat cotidieque pro at, an nullam gloriatur eos. Vel ea scripta singulis, pro et legere commodo.</div><div><br></div><div>Cum ad alterum consetetur, te nam partem postea corpora, altera graeco est ad. Accumsan contentiones vituperatoribus nec ex. Ut mel enim ridens recusabo, eam praesent honestatis ut. Vis adhuc consectetuer mediocritatem ut, tale facete ne sit. Sit apeirian lobortis efficiendi cu, ex illum dicta dicit duo, volumus gloriatur ea ius. Ei pri omittam signiferumque, eos exerci patrioque accommodare ei. Solum platonem necessitatibus ex duo.</div><div><br></div><div>In hinc persius eum, ut dicam graeco eirmod mea. Decore splendide sea id, nam te tibique quaestio convenire. Ex commodo insolens vim. No mei expetendis posidonium liberavisse. Erant comprehensam sed ad, falli periculis ei has. Sit meliore feugait philosophia cu, mei ne debet noluisse sensibus, sed sonet tollit appetere ut.</div><div><br></div><div>An qui unum dicat efficiendi, stet persius oporteat ex vim, eu cum liber gloriatur. An dico ocurreret mel. Velit offendit probatus eam ad. Duo et euismod reformidans, mutat falli dolore id nam, admodum fuisset te mea. Ne debet legere constituto usu, suas mnesarchum intellegebat pro ad.</div><div><br></div>', 1, 1, 1426698000, 1426654304, 1426673469);

-- --------------------------------------------------------

--
-- Struktur dari tabel `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` text NOT NULL,
  `key` varchar(96) NOT NULL,
  `type` int(11) NOT NULL,
  `value` text NOT NULL,
  `option` text NOT NULL,
  `desc` text NOT NULL,
  `sort` int(11) NOT NULL,
  `group` varchar(96) DEFAULT '0',
  `cdate` int(11) NOT NULL,
  `mdate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='config' AUTO_INCREMENT=69 ;

--
-- Dumping data untuk tabel `config`
--

INSERT INTO `config` (`id`, `label`, `key`, `type`, `value`, `option`, `desc`, `sort`, `group`, `cdate`, `mdate`) VALUES
(60, 'About', 'about', 3, 'Attended both Parsons School of Design New York and Paris respectively, he received Silver Thimble Award in 2006 and Bachelor of Fine Arts degree in Fashion Design at 2007. With a passion for structured draperies and intricate embellishments, in addition to sophisticated workmanship, Hediprasetyo remains effortless yet refined in creating his clothing. His designs are to accentuate understated details. It is the intimate luxury of one-on-one relationship with a significant individual that makes his fondness of couture more realistic.', '', '', 0, '1', 1417331772, 1425921884),
(61, 'Personal info', 'personal_info', 3, '<a target="" rel="">contact@didithediprasetyo.com</a><br>\r\n        Ph. +62 878 850 22223', '', '', 0, '1', 1417331772, 1425921834),
(62, 'Press Info', 'press_info', 3, 'Alexandre Boulais Communication<br>\r\n        <a target="" rel="">contact@aboulais.com</a><br>\r\n        Ph. +33 1 53 20 99 98<br>\r\n        24, rue de Cha?teaudun 75009 Paris<br>\r\n        France', '', '', 0, '1', 1417331772, 1425921834),
(63, 'SATKAARA', 'satkaara', 3, '<a target="" rel="">ruthandriani.pm@satkaara.net</a><br>\r\n        Ph. +62 815 138 78007<br>\r\n        Graha Morzell, Jalan Pejaten Raya No.22 Jakarta 12540<br>\r\n        Indonesia', '', '', 0, '1', 1417331772, 1425921834),
(64, 'Facebook Link', 'fb_link', 3, 'https://www.facebook.com/pages/Didit-Hediprasetyo/879043602121246', '', '', 0, '1', 1417331772, 1425921834),
(65, 'Instagram Link', 'ig_link', 3, 'https://instagram.com/didithediprasetyo', '', '', 0, '1', 1417331772, 1425921834),
(66, 'Pinterest Link', 'pin_link', 3, 'https://www.pinterest.com/k123win/didit-hediprasetyo/', '', '', 0, '1', 1417331772, 1425921834),
(67, 'Tumbler Link', 'tblr_link', 3, 'https://www.tumblr.com/search/didit%20couture', '', '', 0, '1', 1417331772, 1425921834),
(68, 'Showcase', 'showcase', 5, '1426235509.jpg', '', '', 0, '1', 1417331772, 1426235509);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `sort` tinyint(4) NOT NULL,
  `mdate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `gallery`
--

INSERT INTO `gallery` (`id`, `article_id`, `image`, `sort`, `mdate`) VALUES
(1, 8, '1426840457.jpg', 2, 1426840457),
(2, 8, '1426840794.jpg', 3, 1426840794),
(4, 8, '1426841240.jpg', 0, 1426841240),
(7, 8, '1426843987.jpg', 1, 1426843987),
(8, 8, '1427116633.png', 4, 1427116633),
(9, 8, '1427116633.png', 5, 1427116633);

-- --------------------------------------------------------

--
-- Struktur dari tabel `meta_group`
--

CREATE TABLE IF NOT EXISTS `meta_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `separated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `meta_group`
--

INSERT INTO `meta_group` (`id`, `name`, `active`, `separated`) VALUES
(1, 'home', 1, 0),
(2, 'playlist', 1, 0),
(3, 'info', 1, 0),
(4, 'landing', 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
